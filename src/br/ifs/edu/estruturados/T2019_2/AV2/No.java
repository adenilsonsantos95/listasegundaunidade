package br.ifs.edu.estruturados.T2019_2.AV2;

public class No<T> {
    private No<T> anterior;
    private T elemento;
    private No<T> proximo;

    public No(T elemento) {
        this.anterior = null;
        this.elemento = elemento;
        this.proximo = null;
    }

    public No getAnterior() {
        return anterior;
    }

    public void setAnterior(No anterior) {
        this.anterior = anterior;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }
}
