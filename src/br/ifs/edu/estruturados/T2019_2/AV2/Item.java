package br.ifs.edu.estruturados.T2019_2.AV2;

import java.util.Objects;

public class Item implements Comparable {
    private Comparable chave;
    private Object informacaoCompleta;

    public Item(){

    }
    public Item(Comparable chave){
        this();
        this.chave = chave;
        this.informacaoCompleta = chave;
    }
    public Item(Comparable chave, Object dado){
        this(chave);
        this.informacaoCompleta = dado;
    }


    public Object getInformacaoCompleta() {
        return informacaoCompleta;
    }

    public void setInformacaoCompleta(Object informacaoCompleta) {
        this.informacaoCompleta = informacaoCompleta;
    }

    public Comparable getChave() {
        return chave;
    }

    public void setChave(Comparable chave) {
        this.chave = chave;
    }

    // Implementar o método compareTo
    public int compareTo(Object o) {
        if (this.compareTo(o) < 0) return -1;
        if (this.compareTo(o) > 0) return  1;
        return 0;
    }

    @Override
    public String toString() {
        return "Item{" +
                "chave=" + chave +
                ", informacaoCompleta=" + informacaoCompleta +
                '}';
    }
}
