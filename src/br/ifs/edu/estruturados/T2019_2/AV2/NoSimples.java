package br.ifs.edu.estruturados.T2019_2.AV2;

public class NoSimples<T> {
    private T elemento;
    private NoSimples proximo;

    public NoSimples(T elemento) {
        this.elemento = elemento;
        this.proximo = null;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public NoSimples getProximo() {
        return proximo;
    }

    public void setProximo(NoSimples proximo) {
        this.proximo = proximo;
    }
}
