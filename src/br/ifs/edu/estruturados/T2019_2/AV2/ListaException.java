package br.ifs.edu.estruturados.T2019_2.AV2;

public class ListaException extends Exception {
    public ListaException(String message) {
        super(message);
    }
}
