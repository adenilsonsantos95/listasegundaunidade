package br.ifs.edu.estruturados.T2019_2.AV2;

public class ListaDuplamenteEncadeada implements ILista{
    private No<Item> primeiro;
    private No<Item> ultimo;
    private int quantidade;
    private int quatidadeMaxima = -1;

    public ListaDuplamenteEncadeada() {
        this.primeiro = null;
        this.ultimo = null;
        this.quantidade = 0;
    }
    public ListaDuplamenteEncadeada(int quatidadeMaxima) {
        this.primeiro = null;
        this.ultimo = null;
        this.quantidade = 0;
        this.quatidadeMaxima = quatidadeMaxima;
    }

    @Override
    public void adicionar(Item dado) throws ListaException {
        if (this.getQtd() == quatidadeMaxima && this.quatidadeMaxima != -1) {
            throw  new ListaException("O dado não pode ser adicionado porque a lista chegou a sua capacidade maxima.");
        } else if(dado == null) {
            throw new ListaException("O dado é nulo.");
        }
        else if(this.getQtd() == 0) {
            No<Item> novo = new No(dado);
            this.primeiro = this.ultimo = novo;
            this.ultimo.setAnterior(this.primeiro);
            this.primeiro.setProximo(this.ultimo);
        }else{
            No<Item> novo = new No(dado);
            this.ultimo.setProximo(novo);
            novo.setAnterior(this.ultimo);
            this.ultimo = novo;
            this.ultimo.setProximo(null);
        }
        this.quantidade++;
    }

    @Override
    public void remover(Comparable chave) throws ListaException {
        if(!this.contem(chave)) { throw new ListaException("Não contém a chave informada.");
        }else if(this.primeiro.getElemento().getChave().compareTo(chave) == 0 && this.getQtd() > 1) {
            this.primeiro = this.primeiro.getProximo();
            this.primeiro.setAnterior(null);
        }else if(this.ultimo.getElemento().getChave().compareTo(chave) == 0 && this.getQtd() > 1) {
            this.ultimo = this.ultimo.getAnterior();
            this.ultimo.setProximo(null);
        }else if (this.primeiro.getElemento().getChave().compareTo(chave) == 0 && this.getQtd() == 1){
            this.primeiro = this.ultimo = null;
        } else {
            No<Item> aux = this.primeiro.getProximo();
            for(int i = 1; i < this.getQtd()-1; i++) {
                if(aux.getElemento().getChave().compareTo(chave) == 0) {
                    aux.getAnterior().setProximo(aux.getProximo());
                    aux.getProximo().setAnterior(aux.getAnterior());
                    break;
                }
                aux = aux.getProximo();
            }
        }
        this.quantidade--;
    }

    @Override
    public boolean contem(Comparable chave) throws ListaException {
        if (chave == null) throw new ListaException("O elemento passado é nulo.");
        No<Item> aux = this.primeiro;
        for (int i = 0; i < this.getQtd(); i++) {
            if (chave.compareTo(aux.getElemento().getChave()) == 0){ return true;}
            aux = aux.getProximo();
        }
        return false;
    }

    @Override
    public int getQtd() {return this.quantidade;}

    @Override
    public void AdicionarNoFinal(ILista outralista) throws ListaException {
        if (outralista.getQtd() == 0) throw new ListaException("A lista a ser adicionada está vazia.");
        else{
            Item[] novo = outralista.getSubLista(0, outralista.getQtd()-1);
            for (int i = 0; i < novo.length; i++) {this.adicionar(novo[i]);}
        }
    }

    private Item obterDaPosicao(int posicao){
        No<Item> aux = this.primeiro;
        for (int i = 0; i < this.getQtd(); i++) {
            if ( i == posicao){break;}
            aux = aux.getProximo();
        }
        return aux.getElemento();
    }

    @Override
    public void AdicionarNoFinal(Item[] outralista) throws ListaException {
        if (outralista.length == 0) throw new ListaException("A lista a ser adicionada está vazia.");
        for (int i = 0; i < outralista.length; i++) {
            this.adicionar(outralista[i]);
        }
    }

    @Override
    public void AdicionarNoMeio(Comparable chaveDoItemDoMeio, ILista outralista) throws ListaException {
        if (this.getQtd() == 0 || outralista.getQtd() == 0){throw new ListaException("A lista orignal ou a outra lista está vazia.");}
        if (chaveDoItemDoMeio == null) throw new ListaException("A chave passada é nula.");
        if (!this.contem(chaveDoItemDoMeio)) throw new ListaException("A chave passada não existe na lista.");
        if (this.ultimo.getElemento().getChave().compareTo(chaveDoItemDoMeio) == 0){
            this.AdicionarNoFinal(outralista);
            return;
        }
        No<Item> aux = this.primeiro;
        for (int i = 0; i < this.getQtd()-1; i++){
            No<Item> proximo = aux.getProximo();
            if (chaveDoItemDoMeio.compareTo(aux.getElemento().getChave()) == 0){
                Item[] novo = outralista.getSubLista(0, outralista.getQtd()-1);
                int j;
                for (j = 0; j < novo.length-1; j++) {
                    No<Item> temp = new No<>(novo[j]);
                    aux.setProximo(temp);
                    aux.getProximo().setAnterior(aux);
                    aux = aux.getProximo();
                    this.quantidade++;
                }
                No<Item> temp2 = new No<>(novo[j]);
                aux.setProximo(temp2);
                aux.getProximo().setAnterior(aux);
                aux.getProximo().setProximo(proximo);
                aux.getProximo().getProximo().setAnterior(temp2);
                this.quantidade++;
                return;
            }
            aux = aux.getProximo();
        }
    }

    @Override
    public Item[] getSubLista(int posicaoInicial, int posicaoFinal) throws ListaException {
        if (posicaoInicial < 0 || posicaoFinal >= this.getQtd()) throw new ListaException("Posição inicial e/ou Posição final está(am) incorreta(s).");
        if (posicaoInicial > posicaoFinal) throw new ListaException("A posição inicial é maior que a posição final");
        Item[] novo = new Item[posicaoFinal - posicaoInicial + 1];
        int aux = posicaoInicial;
        for (int i = 0; i < novo.length; i++) {
            novo[i] = obterDaPosicao(aux);
            aux++;
        }
        return novo;
    }

    @Override
    public int getQtdElementos(Item item) throws ListaException {
        if (this.getQtd() == 0) throw new ListaException("A lista está vazia.");
        No<Item> aux = this.primeiro;
        int contador = 0;
        for (int i = 0; i < this.getQtd(); i++) {
            if (item.getChave() == aux.getElemento().getChave() && item.getInformacaoCompleta() == aux.getElemento().getInformacaoCompleta()){
                contador = contador + 1;
            }
            aux = aux.getProximo();
        }
        return contador;
    }

    @Override
    public void removerDuplicados() throws ListaException {
        if (this.getQtd() == 1) return;
        if (this.getQtd() == 0) throw  new ListaException("A lista está vazia.");
        Item aux = null;
        int contador = this.getQtd();
        for (int i = 0; i < contador; i++) {
            aux = this.obterDaPosicao(i);
            while(this.getQtdElementos(aux) != 1){
                this.remover(aux.getChave());
                contador -= 1;
            }
        }
    }

    @Override
    public String toString() {
        if (this.getQtd() == 0) return "ListaDuplamenteEncadeada[]";
        else{
            String lista = "ListaDuplamenteEncadeada[";
            No<Item> aux = this.primeiro;
            for (int i = 0; i < this.getQtd()-1; i++) {
                lista = lista + aux.getElemento()  + " - " ;
                aux = aux.getProximo();
            }
            lista = lista + aux.getElemento() + "]";
            return lista;
        }
    }
}
